function compareDates(date1, date2){
    date1 = splitDate(date1);
    date2 = splitDate(date2);
    if(date1 === date2)
        return true;
    else return false;
}

function splitDate(date){
    dateArr = date.split(/\/|\.|-|,/g);
    dateArr.sort((a, b) => {return a-b;});
    dateArr = dateArr.map(e => e.trim());
    let res = dateArr.join(".");
    return res;
}

console.log(compareDates('01.02.2541', '01 ,02,2541 '));