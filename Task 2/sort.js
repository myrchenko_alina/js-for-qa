function superSort(value){
    let values = value.split(" ");

    values.sort(function(a, b){
        let aNoNum = a.replace(/\d/g, '');
        let bNoNum = b.replace(/\d/g, '');
        if(aNoNum < bNoNum)
            return -1;
        if(aNoNum > bNoNum)
            return 1;
        return 0;
    });

    let res = values.join(' ');
    console.log(res);
};

superSort('mic09ha1el 4b5en6 michelle be4atr3ice');