setTimeout(() => {
    const bubbles = document.getElementsByClassName('bubble');
    const bubbleNum = bubbles.length;
    Array.from(bubbles).forEach(b => {b.click()});
    const score = $('div#score');
    expect(Number(score.getText())).to.equal(bubbleNum, 'Score doesn\'t match bbubbles clicked')
}, 5000)